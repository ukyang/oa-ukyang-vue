import Vue from 'vue'
import Vuex from 'vuex'
import {getRequest} from "@/utils/api";
// ws 相关导包
import SockJS from 'sockjs-client'
import Stomp from 'stompjs'
// 导入 elementui 弹出消息框相关组件
import { Notification } from 'element-ui';

Vue.use(Vuex)

const now = new Date();

const store = new Vuex.Store({
    // 全局对象，用来保存所有组件的公共数据
    state: {
        // 整个菜单最后都是渲染成路由的，所以这里先准备一个路由
        routes: [],
        // 这里存放聊天记录，原来是数组，我这里改成对象
        // {'zhangsan#lisi':[{}]}，我计划以这样的格式存储聊天记录
        sessions: {},
        // 所有操作员，不包含当前操作员
        admins: [],
        // 当前操作员，正常获取，可能有问题，有问题再回来改
        // 果然出问题了，这个全局属性不一定能获取到，因为是从sessionStorage中获取的，所以可能会有一些延迟情况
        // 所以要把这个属性的赋值提前
        // 既然要提前，那干脆把该属性的赋值放到全局前置守卫中
        // 后来又出现一样的问题。。那就再改回去，一个不行就试另一个
        // 最终决定两个都加，保险起见
        // currentAdmin: null,
        currentAdmin: JSON.parse(window.sessionStorage.getItem('user')),
        // 原先这只是一个用户 id，但我现在能获得所有操作员的数据，所以我直接把它换成对象
        // 全局替换，记得区分大小写
        currentSession: null,
        filterKey: '',
        stomp: null,
        // 是否是小红点状态
        // 定义为一个对象，里面的内容和 sessions 中的格式是一样的
        // 然后最后是 true 就展示小红点，false 就不展示小红点
        isDot: {}
    },

    // 这是唯一可以改变 state 中对应值的方法，不过这个方法是同步执行的
    // 所以第一步就是初始化路由
    mutations: {
        initRoutes(state, data) {
            state.routes = data
        },
        changeCurrentSession(state, user) {
            state.currentSession = user;
            // 小红点取消
            if (user !== null) {
                Vue.set(state.isDot, state.currentAdmin.username + '#' + user.username, false);
            }
        },
        addMessage(state, msg) {
            // ws，消息的发送
            let mss = state.sessions[state.currentAdmin.username + '#' + msg.to]
            if (!mss) {
                // 为了让 vue 主动监听 session 的变化，这里的写法要修改一下
                // state.sessions[state.currentAdmin.username + '#' + msg.to] = []
                Vue.set(state.sessions,state.currentAdmin.username + '#' + msg.to,[])
            }
            state.sessions[state.currentAdmin.username + '#' + msg.to].push({
                content: msg.content,
                date: new Date(),
                // 我#我 这种情况是不允许出现的
                // 也就是说，要把发送出去的消息和接收的消息区分开来
                // 接收的消息 self 为 false
                // 发送的消息 self 为 true
                // 最终的情况就全部是：我 # 对方，然后通过 self 来区分谁是主语
                self: !msg.notSelf
            });
        },
        INIT_DATA(state) {
            // 浏览器本地历史聊天记录
            // ws 消息的展示以及持久化
            let data = localStorage.getItem('vue-chat-session');
            //console.log(data)
            if (data) {
                state.sessions = JSON.parse(data);
            }
        },
        INIT_ADMINS(state, data) {
            state.admins = data
        },
        INIT_CURRENTADMIN(state, admin) {
            state.currentAdmin = admin
        },
    },

    // 异步执行的方法，先不设置，后面要用的时候再说
    actions: {
        initData(context) {
            // ws 消息持久化
            context.commit('INIT_DATA')
            getRequest('/chat/admins').then(resp => {
                if (resp) {
                    context.commit('INIT_ADMINS', resp)
                }
            })
            // context.commit('INIT_DATA')
        },
        connect(context) {
            // 先在后端对应的端点建立连接
            context.state.stomp = Stomp.over(new SockJS('/ws/ep'));
            // 因为后端用了 JWT 令牌技术，并且设置了 JWT 登录拦截器，所以前端要传 TOKEN 到后端进行验证
            let token = window.sessionStorage.getItem('tokenStr')
            // 现在把整个连接发送到后端进行验证，包括 TOKEN，TOKEN 是通过 key-value 的形式传到后端的，key 的名称自定义，但是后端代码要对应上
            context.state.stomp.connect({'Auth-Token': token}, success => {
                // 连接成功，那客户端要进行消息订阅，这个 user 路径是固定格式
                // 订阅后就能通过回调从后端拿到消息了
                context.state.stomp.subscribe('/user/queue/chat', msg => {
                    let receiveMsg = JSON.parse(msg.body);
                    // ws，消息的接收 + 消息弹框
                    // 在生产环境下要改这里
                    if (!context.state.currentSession || receiveMsg.from !== context.state.currentSession.username || window.location.href !== 'http://localhost:8080/#/chat') {
                        // js 文件不能直接用 this.$ 的写法，所以直接拷贝过来是不行的
                        Notification.info({
                            title: '【'+receiveMsg.fromNickName+'】 发来一条消息',
                            message: receiveMsg.content.length > 10 ? receiveMsg.content.substr(0, 10) : receiveMsg.content,
                            position: 'bottom-right'
                        });
                        Vue.set(context.state.isDot, context.state.currentAdmin.username + '#' + receiveMsg.from, true);
                    }
                    receiveMsg.notSelf = true
                    receiveMsg.to = receiveMsg.from
                    context.commit('addMessage',receiveMsg)
                })
            }, error => {
                console.log(error)
            })
        },
    }
})
// ws 消息持久化监听，一旦 sessions 有变化，就会存进 localStorage
// 然后配合上面的 initData 和 INIT_DATA 方法，在 FriendChat 组件中进行调用
store.watch(function (state) {
    return state.sessions
}, function (val) {
    console.log('CHANGE: ', val);
    localStorage.setItem('vue-chat-session', JSON.stringify(val));
}, {
    deep: true/*这个貌似是开启watch监测的判断,官方说明也比较模糊*/
})

export default store