import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: '登录页面',
        component: () => import('../views/Login'),
        // hidden: true
    },
    {
        path: '/home',
        name: '非权限页面',
        component: () => import('../views/Home'),
        children: [
            {
                path: '/chat',
                name: '在线聊天',
                component: () => import('../views/chat/FriendChat'),
                // hidden: true
            },
            {
                path: '/userinfo',
                name: '个人中心',
                component: () => import('../views/AdminInfo'),
                // hidden: true
            }
        ]
    }
]

const router = new VueRouter({
    routes,
    // mode: 'history'
})

export default router
