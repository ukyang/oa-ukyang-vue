import axios from "axios"

// 给 axios 带一个默认的响应类型，因为接收到的是流，流的话肯定是二进制数组
// 然后该配置文件的意思就是，针对 responseType 为 arraybuffer 的接口设置拦截器
// 本质上就是对 content-type 为 application/octet-stream 的响应进行拦截处理
const service = axios.create({
    responseType: 'arraybuffer'
})

// 请求拦截器，一般请求和响应的拦截器都是成对写的，所以这里例行公事写一下
service.interceptors.request.use(config => {
    config.headers['Authorization'] = window.sessionStorage.getItem('tokenStr')
    return config
}, error => {
    console.log(error)
})

// 响应拦截器
service.interceptors.response.use(resp => {
    const headers = resp.headers
    // 后端大部分返回的都是 JSON 字符串，只有下载文件这种特殊的接口才会是流的形式，但也可能有例外情况：
    // 返回的是一个 arraybuffer 类型，但是响应头并不是 application/octet-stream
    // 所以这里为了健壮性考虑，需要进行一下判断
    let reg = RegExp(/application\/json/);
    if (headers['content-type'].match(reg)) {
        // 匹配，说明这就是一个普通的接口，不是下载文件的接口
        // 那么就转成 JSON 字符串类型返回，因为 JSON 才是标准返回格式
        // 一般这个分支不会走，因为我在后端并没有设计这么奇怪的接口
        resp.data = unitToString(resp.data);
    } else {
        // 不匹配，说明返回的是一个文件流，就是我想要的拦截，这里就开始声明我引入的插件
        let fileDownload = require('js-file-download')
        // 这里要拿到文件名，可以通过响应头来获得，具体的格式可以看后端，这是固定的，百度即可查
        let fileName = headers['content-disposition'].split(';')[1].split('filename=')[1]
        // 响应类型，这其实是固定的，就是 application/octet-stream，因为我没有设置其他的奇怪的接口
        let contentType = headers['content-type']

        // 拿到上面的三个变量后，开始进行下面的操作
        // 将文件名格式化，防止乱码
        fileName = decodeURIComponent(fileName)
        // 这里就是利用插件，将三个参数放进去，直接启用插件就 ok 了
        // 意思就是，通过 js-file-download 插件去下载文件
        fileDownload(resp.data, fileName, contentType)
    }
}, error => {
    console.log(error)
})

function unitToString(unitArray) {
    // 编码
    let encodedString = String.fromCharCode.apply(null, new Uint8Array(unitArray))
    // 解码
    let decodedString = decodeURIComponent(escape(encodedString))
    return JSON.parse(decodedString)
}

// base 的设置也是例行公事
let base = ''

// 专门封装一个下载的请求
// 后端关于下载的请求设计的就是 get 请求
// 一般情况下，跟下载有关的请求都是 get 请求
export const downloadRequest = (url, params) => {
    return service({
        method: 'get',
        url: `${base}${url}`,
        data: params
    })
}

// export default service