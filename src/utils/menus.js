import {getRequest} from "@/utils/api";
import vueRouterEsm from "vue-router";

// 对 vuex 中的 state 进行初始化
export const initMenu = (router, store) => {
    // 如果 vuex 中的 state 已经有值了，那么就没必要初始化了
    if (store.state.routes.length > 0) {
        return
    }

    getRequest('/emp/basic/menu').then(data => {
        // 从后端接口拿到数据，第一步就是去判断数据是否存在
        if (data) {
            // 如果数据存在，那么就格式化数据
            let fmtRoutes = formatRoutes(data)
            let fmtRoute = {
                path: '*',
                component: () => import('../views/NotFound'),
                hidden: true
            }
            fmtRoutes.push(fmtRoute)
            // 将格式化 routes 添加到路由中
            router.addRoutes(fmtRoutes)
            // 将数据存到 vuex 里
            store.commit('initRoutes', fmtRoutes)
            // 连接 ws
            store.dispatch('connect')
        }
    })
}

export const formatRoutes = (data) => {
    let fmtRoutes = [];
    data.forEach(route => {
        let {
            path,
            component,
            name,
            iconCls,
            children
        } = route;

        if (children && children instanceof Array) {
            // 递归
            children = formatRoutes(children)
        }
        let fmtRoute = {
            path: path,
            name: name,
            iconCls: iconCls,
            children: children,
            component(resolve) {
                if (component.startsWith('Home')) {
                    require((['../views/' + component + '']), resolve);
                } else if (component.startsWith('Emp')) {
                    // es5 写法，用 es6 写法也行
                    // 这里就从字符串着为组件了，而且利用了类似懒加载的机制，这在默认的路由写法里也有体现
                    require((['../views/emp/' + component + '']), resolve);
                } else if (component.startsWith('Per')) {
                    require((['../views/per/' + component + '']), resolve);
                } else if (component.startsWith('Sal')) {
                    require((['../views/sal/' + component + '']), resolve);
                } else if (component.startsWith('Sta')) {
                    require((['../views/sta/' + component + '']), resolve);
                } else if (component.startsWith('Sys')) {
                    require((['../views/sys/' + component + '']), resolve);
                }
            }
        }
        fmtRoutes.push(fmtRoute)
    })

    return fmtRoutes
}