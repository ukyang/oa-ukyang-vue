import axios from 'axios'
// import {Message} from 'element-ui'
import {message} from "@/utils/resetMessage";
import router from "../router"

// 请求拦截器
axios.interceptors.request.use(config => {
    // 如果存在 TOKEN，请求携带这个 TOKEN
    if (window.sessionStorage.getItem('tokenStr')) {
        config.headers['Authorization'] = window.sessionStorage.getItem('tokenStr')
    }
    // if (window.sessionStorage.getItem('user')) {
    //     getRequest('/admin/info').then(resp => {
    //         // 判断能否拿到 resp
    //         if (resp) {
    //         }else{
    //             window.sessionStorage.clear()
    //             router.replace('/')
    //         }
    //     })
    // }
    return config
}, error => {
    // 这里就简单一点，因为不怎么用的到
    console.log(error)
})

// 响应拦截器
axios.interceptors.response.use(success => {
    // 业务逻辑判断
    if (success.status && success.status === 200) {
        if (success.data.code === 500 || success.data.code === 401 || success.data.code === 403) {
            message.error({message: success.data.message})
            return
        }
        if (success.data.code === 444) {
            message.error({message: success.data.message})
            window.sessionStorage.clear()
            router.replace('/')
            return
        }
        if (success.data.message) {
            message.success({message: success.data.message});
        }
    }
    return success.data
}, error => {
    if (error.response.code === 504 || error.response.code === 404) {
        message.error({message: "服务器被吃了，大概率宕机，小概率破产，管理员可能跑路"})
    } else if (error.response.code === 403) {
        message.error({message: "权限不足，请联系管理员"})
    } else if (error.response.code === 401) {
        message.error({message: "尚未登录，请登录"})
        router.replace('/')
    } else {
        if (error.response.data.message) {
            message.error({message: error.response.data.message});
        } else {
            message.error({message: "未知错误"})
        }
    }
})


let base = ''
// 传送 json 格式的 post 请求
export const postRequest = (url, params) => {
    return axios({
        method: 'post',
        url: `${base}${url}`,
        data: params
    })
}

// 传送 json 格式的 get 请求
export const getRequest = (url, params) => {
    return axios({
        method: 'get',
        url: `${base}${url}`,
        data: params
    })
}

// 传送 json 格式的 put 请求
export const putRequest = (url, params) => {
    return axios({
        method: 'put',
        url: `${base}${url}`,
        data: params
    })
}

// 传送 json 格式的 delete 请求
export const deleteRequest = (url, params) => {
    return axios({
        method: 'delete',
        url: `${base}${url}`,
        data: params
    })
}

