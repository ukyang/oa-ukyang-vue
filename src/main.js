import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI,{Message} from 'element-ui'
// 引入自定义 resetMessage
import {message} from '@/utils/resetMessage'
// import 'element-ui/lib/theme-chalk/index.css'
import '../theme/theme01/index.css'
import {postRequest} from "./utils/api"
import {getRequest} from "./utils/api"
import {putRequest} from "./utils/api"
import {deleteRequest} from "./utils/api"
import {downloadRequest} from "@/utils/download"
import {initMenu} from "@/utils/menus";
// font-awesome 的 css 文件
import 'font-awesome/css/font-awesome.css'
// import message from "element-ui/packages/message";

Vue.config.productionTip = false
Vue.use(ElementUI, {size: 'small'})

// 插件形式使用请求
// 挂载到 Vue 全局对象上

Vue.prototype.postRequest = postRequest
Vue.prototype.getRequest = getRequest
Vue.prototype.putRequest = putRequest
Vue.prototype.deleteRequest = deleteRequest
Vue.prototype.downloadRequest = downloadRequest
Vue.prototype.$message = message

router.beforeEach((to, from, next) => {
        // 判断用户是否登录
        if (window.sessionStorage.getItem('tokenStr')) {
            initMenu(router, store);
            // 判断完成后需要继续判断，用户信息是否存在
            if (!window.sessionStorage.getItem('user')) {
                return getRequest('/admin/info').then(resp => {
                    // 判断能否拿到 resp
                    if (resp) {
                        // sessionstorage 中只能存储字符串，所以记得转换
                        window.sessionStorage.setItem('user', JSON.stringify(resp))
                        // 初始化一下 vuex 中的数据，双重保险
                        store.commit('INIT_CURRENTADMIN',resp)
                        next()
                    }
                })
            }else{
                getRequest('/admin/info').then(resp => {
                    // 判断能否拿到 resp
                    if (resp) {
                        next()
                    }else{
                        window.sessionStorage.clear()
                        message.error({message: "账号被禁用，请联系管理员"})
                        next('/')
                    }
                })
            }
            next();
            // return getRequest('/admin/info').then(resp => {
            //     // 判断能否拿到 resp
            //     if (resp) {
            //         // sessionstorage 中只能存储字符串，所以记得转换
            //         window.sessionStorage.setItem('user', JSON.stringify(resp))
            //         next()
            //     }
            //     next()
            // })
        } else {
            // 如果没有登录，但是是去登录页，那还是跳转
            if (to.path === '/') {
                next();
            } else {
                // 如果没有登录，而且是想要直接去某个 url，那么跳转到登录页并传个重定向的参数即可
                next('/?redirect=' + to.path)
            }
        }

    }
)


new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
