// const { defineConfig } = require('@vue/cli-service')
// module.exports = defineConfig({
//   transpileDependencies: true
// })

let proxyObj={}

// nodejs 代理
proxyObj['/'] = {
  // websocket
  ws:false,
  // 目标地址
  target: 'http://localhost:8090',
  // 发送请求头 host 会被设置成 target
  changeOrigin: true,
  // 不重写请求地址
  pathReWrite: {
    '^/': ''
  }
}

// ws 代理
proxyObj['/ws'] ={
  ws: false,
  target: 'ws://localhost:8090'
}

module.exports = {
  devServer:{
    host: 'localhost',
    port: 80,
    https: false,
    proxy: proxyObj,
  },
  chainWebpack: config => {
    config.module
        .rule("md")
        .test(/\.md/)
        .use("vue-loader")
        .loader("vue-loader")
        .end()
        .use("vue-markdown-loader")
        .loader("vue-markdown-loader/lib/markdown-compiler")
        .options({
          raw: true,
        });
  },
}